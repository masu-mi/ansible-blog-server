#!/bin/bash

# 設定
HTTP="80"
HTTPS="443"
SSH="22"

# 変数
IPTABLES=${IPTABLES-"iptables"}
LOG_PROGNAME=$IPTABLES
LOG_LEVEL=${LOG_LEVEL-"warning"}

# 関数
function resolv {
    local result
    result=$(dig +short $1)
    if [ -n "$result" ]; then
        echo $result
    else
        echo $1
    fi
}

# 変数自動設定
BROADCAST=$(ip -f inet addr show dev eth0 | grep brd | awk '{print $4}')
NAMESERVERS=($(grep '^nameserver' /etc/resolv.conf | grep -v '::' | cut -d' ' -f2))
NTPSERVERS=($(grep '^server' /etc/ntp.conf | cut -d' ' -f2 | while read ntpserver; do resolv $ntpserver; done))

[ "$1" = "-n" ] && IPTABLES="echo ${IPTABLES}"

# 初期化
$IPTABLES -F  # ルールをクリア
$IPTABLES -X  # チェーンを削除

# デフォルトポリシー
# DROP にするとルールをクリアした時点で通信不能になるので ACCEPT
$IPTABLES -P INPUT   ACCEPT
$IPTABLES -P OUTPUT  ACCEPT
$IPTABLES -P FORWARD DROP

# 信頼するホストの通信を許可
$IPTABLES -A INPUT -i lo -j ACCEPT

# セッション確立後のパケットを許可
$IPTABLES -A INPUT -p tcp -m state --state ESTABLISHED,RELATED -j ACCEPT


# フラグメントパケットを破棄
$IPTABLES -A INPUT -f -j LOG --log-level $LOG_LEVEL --log-prefix "$LOG_PROGNAME[fragment] "
$IPTABLES -A INPUT -f -j DROP


# 名前解決できるようにする
# これを許可しないと逆引きをする多くのソフトの動作が遅くなる。
# ssh/scp で VPS へのログインが遅くなるのもこれが原因。
for nameserver in ${NAMESERVERS[@]}; do
    $IPTABLES -A INPUT -s $nameserver -p udp --sport 53 -j ACCEPT
done

# NTP サーバと同期できるようにする
for ntpserver in ${NTPSERVERS[@]}; do
    $IPTABLES -A INPUT -s $ntpserver -p udp --sport 123 --dport 123 -j ACCEPT
done


# SSH
$IPTABLES -N SSH
# 攻撃対策: SSH brute force
$IPTABLES -N SSH_BRUTE_FORCE


# SSH_BLOCKED に登録して破棄。攻撃は初回のみログに記録
$IPTABLES -A SSH_BRUTE_FORCE -m recent --name SSH_BLOCKED --set
$IPTABLES -A SSH_BRUTE_FORCE -j LOG --log-level $LOG_LEVEL --log-prefix "$LOG_PROGNAME[SSH attack] "
$IPTABLES -A SSH_BRUTE_FORCE -j DROP

# SSH_BLOCKED に登録されていて 600 秒以内にアクセスがあれば破棄
$IPTABLES -A SSH -m recent --name SSH_BLOCKED --update --seconds 600 --rttl -j DROP

# 60 秒間に 8 回以上のアクセスがあれば攻撃と見なしてブロックリストに登録
$IPTABLES -A SSH -m recent --name SSH --set
$IPTABLES -A SSH -m recent --name SSH --rcheck --seconds 60 --hitcount 8 --rttl -j SSH_BRUTE_FORCE

$IPTABLES -A SSH -j ACCEPT
$IPTABLES -A INPUT -p tcp --syn --dport $SSH -j SSH


# hashlimit モジュールの説明
# --hashlimit-above  パケットがこの頻度を超えて到達したらルールにマッチする
# --hashlimit-burst  バーストに備えて繰り越すパケット数の最大値であり、初期値。
#                    繰り越し値を消費しきるまでは制約にマッチしない。
#                    --hashlimit-above の制約にマッチしない度に 1 ずつ繰り越される。
# --hashlimit-mode   制限を管理する単位。
# --hashlimit-name   テーブルの名前。/proc/net/ipt_hashlimit/<name> で参照できる。
# --hashlimig-htable-expire 指定したミリ秒間にパケットが到達しなければテーブルをリセットする。


# ICMP
# 攻撃対策: Ping of Death
$IPTABLES -N PING_OF_DEATH
$IPTABLES -A PING_OF_DEATH -j LOG --log-level $LOG_LEVEL --log-prefix "$LOG_PROGNAME[PoD attack] "
$IPTABLES -A PING_OF_DEATH -j DROP

$IPTABLES -A INPUT -p icmp --icmp-type echo-request \
          -m hashlimit --hashlimit-above 10/second --hashlimit-burst 10 \
          --hashlimit-mode dstip --hashlimit-name in_icmp \
          --hashlimit-htable-expire 300000 \
          -j PING_OF_DEATH

$IPTABLES -A INPUT -p icmp -j ACCEPT


# 攻撃対策: SYN flood
$IPTABLES -N SYN_FLOOD
$IPTABLES -A SYN_FLOOD -j LOG --log-level $LOG_LEVEL --log-prefix "$LOG_PROGNAME[SYN flood attack] "
$IPTABLES -A SYN_FLOOD -j DROP

$IPTABLES -A INPUT -p tcp --syn \
          -m hashlimit --hashlimit-above 20/second --hashlimit-burst 50 \
          --hashlimit-mode srcip --hashlimit-name in_syn \
          --hashlimit-htable-expire 300000 \
          -j SYN_FLOOD


# ブロードキャスト、マルチキャストを破棄
# Dropbox のパケット udp/17500 もこれでブロック
$IPTABLES -A INPUT -d $BROADCAST -j DROP
$IPTABLES -A INPUT -d 255.255.255.255 -j DROP
$IPTABLES -A INPUT -d 224.0.0.0/4 -j DROP


# アクセスを許可
$IPTABLES -A INPUT -p tcp -m multiport --dports $HTTP  -j ACCEPT
$IPTABLES -A INPUT -p tcp -m multiport --dports $HTTPS -j ACCEPT


# 未定義はすべて拒否
$IPTABLES -A INPUT -j LOG --log-level $LOG_LEVEL --log-prefix "$LOG_PROGNAME[Drop] "
$IPTABLES -A INPUT -j DROP
