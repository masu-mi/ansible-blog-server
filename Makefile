
ssh_conf_path = ~/.ssh/
files_path    = ./files/
tls_path      = $(files_path)/tls/
pub_keys_path = $(files_path)/gitolite/users/ssh/

.PHONY: setup

setup: roles $(pub_keys_path)masumi.pub $(tls_path)chain.pem $(tls_path)fullchain.pem $(tls_path)privkey.pem
	ansible-galaxy -r ./requirements.yml -p $< install

roles:
	mkdir $@

$(tls_path)chain.pem: $(tls_path)crt.pem
	cp $< $@

$(tls_path)fullchain.pem: $(tls_path)crt.pem
	cp $< $@

$(tls_path)crt.pem: $(tls_path)csr.pem $(tls_path)privkey.pem
	openssl x509 -days 365 -req -signkey $(tls_path)privkey.pem < $(tls_path)csr.pem > $@

$(tls_path)csr.pem: $(tls_path)privkey.pem
	openssl req -sha256 -newkey rsa:2048 -new -key $< > $@

$(tls_path)privkey.pem:
	openssl genrsa 4096 > $@

$(pub_keys_path)masumi.pub:
	cp $(ssh_conf_path)id_rsa.pub $@
